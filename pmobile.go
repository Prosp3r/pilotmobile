package main

import (
	fmt "fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
	psocket "gitlab.com/prosp3r/pilotmobile/psocket"
)

//"log"
//"github.com/golang/protobuf/proto"

//DEFINE our message object
type Message struct {
	Email    string `json:"email"`
	Username string `json:"username"`
	Message  string `json:"message"`
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func handler(w http.ResponseWriter, r *http.Request) {
	//UPGRADE OPEN CONNECTION TO SOCKET CONN
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	//READ/WRITE TO CONNECTION CONN
	for {
		//READ WITH conn
		messageType, p, err := conn.ReadMessage()
		if err != nil {
			log.Println(err)
			return
		}
		//WRITE to connection WITH conn
		if err := conn.WriteMessage(messageType, p); err != nil {
			log.Println(err)
			return
		}
	}

	//

}

func homePage(w http.ResponseWriter, r *http.Request) {
	//
	fmt.Fprintf(w, "Hello Binshes")
}

func userInfo(w http.ResponseWriter, r *http.Request) {
	ws, err := psocket.Upgrade(w, r)
	if err != nil {
		fmt.Fprintf(w, "%-v\n", err)
	}
	go psocket.Writer(ws)
}

//SOCKET EXAMPLES
func main() {
	//
	setupRoutes()
}

func setupRoutes() {
	http.HandleFunc("/", homePage)
	http.HandleFunc("/user", userInfo)
	log.Fatal(http.ListenAndServe(":8089", nil))
}

/*
//gRPC EXAMPLES
func main() {
	//first mobile code
	me := &Device {
		Name: "iPhone",
		TotalCapacity: 16.50,
		AvailableCapacity: 3.05,
		Version: "9.3.5(13G36)",
		BlueTooth: "30:F7:C5:9E:85:F1",
	}

	data, err := proto.Marshal(me)
	if err != nil {
		log.Fatal("marshaling error: ", err)
	}

	fmt.Println(data)

	//REVERSE
	newMe := &Device{}
	err = proto.Unmarshal(data, newMe)
	if err != nil {
		log.Fatal("unmarshalling err: ", err)
	}
	fmt.Println(newMe.GetName())
	fmt.Println(newMe.GetAvailableCapacity())
	fmt.Println(newMe.GetVersion())
	fmt.Println(newMe.GetBlueTooth())
}*/
