//TCPUDP SERVER

package socketServer

import (
	fmt "fmt"
	"net"
)

var maxBufferSize = 1024;

//NewTCPSocket creates a new TCP SOCKET SERVER INSTANCE
func NewTCPSocket() {
	//initialize
	tcpAddr, err := net.ResolveTCPAddr(resolver, serverAddr)
	if err != nil {
		fmt.Println(err.Error())
	}

	//LISTENER
	listener, err := net.ListeneTCP("tcp", tcpAddr)
	if err != nil {
		//
		fmt.Println(err.Error())
	}
	//listen for incoming connection 
	conn, err := listener.Accept()
	if err != nil {
		fmt.Println(err.Error())
	}

	//send message
	if _, err := conn.Write({message}); err != nil {
		fmt.Println(err.Error())
	}

	//receive message
	buf := make([]byte, 1024)
	n, err := conn.Read(buf[0:])
	if err != nil {
		fmt.Println(err.Error())
	}
}

//NewUDPSocket creates a new UDP SOCKET SERVER INSTANCE
func NewUDPSocket(address string) {
	//initialize
	udpAddr, err := net.ResolveUDPAddr(resolver, serverAddr)
		if err != nil {
			fmt.Println(err.Error())
		}
	
	conn, err := net.ListenUDP("udp", udpAddr)
	if err != nil {
		fmt.Println(err.Error())
	}

	//SEND MESSAGE
	buffer := make([]byte, maxBufferSize)
	n, addr, err := conn.ReadFromUDP(buffer)
	if err != nil {
		fmt.Println(err.Error())
	}

	//RECEIVE MESSAGE
	buffer := make([]byte, maxBufferSize)
	n, err = conn.WriteToUDP(buffer[:n], addr)
	if err != nil {
		fmt.Println(err.Error())
	}
}