//TCPUDP SOCKET CLIENT

package socketClient

import (
	fmt "fmt"
	"net"
)

var maxBufferSize = 1024

//NewTCPSocket creates a new tcp socket client
func newTCPSocket(serverAddr, resolver string) {
	tcpAddr, err := net.ResolveTPCAddr(resolver, serverAddr)
	if err != nil {
		fmt.Println(err.Error())
	}
	conn, err := net.DialTCP(network, nil, tcpAddr)
	if err != nil {
		fmt.Println(err.Error())
	}

	//receive message
	var buf [1024]byte
	_, err = conn.Read(buf[0:])
	if err != nil {
		fmt.Println(err.Error())
	}
}

//NewUDPSocket creates a new udp socket client
func NewUDPSocket(address string) {
	//initialize
	udpAddr, err := net.ResolveUDPAddr("udp", address)
	if err != nil {
		fmt.Println(err.Error())
	}

	//LISTENER
	conn, err := net.DialUDP("udp", nil, udpAddr)
	if err != nil {
		//
		fmt.Println(err.Error())
	}

	//send message
	buffer := make([]byte, maxBufferSize)
	n, addr, err := conn.ReadFrom(buffer)

	if err != nil {
		fmt.Println(err.Error())
	}

	//receive message
	buffer := make([]byte, maxBufferSize)
	n, err := conn.Read(buf[0:])
	if err != nil {
		fmt.Println(err.Error())
	}
}
